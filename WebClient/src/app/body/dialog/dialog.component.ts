import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'modal-content',
  template: `
    <div class="modal-header hdrblue">
      <h4 class="modal-title pull-left text-title">{{title}}</h4>
    </div>
    <div class="modal-body row">
      <div class="col-12">
        {{content}}
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" (click)="Ok()">{{btnTittle}}</button>
    </div>
  `
})
 
export class ConfirmDialogComponent implements OnInit {
  title: string;
  content: string;
  btnTittle: string;
  @Output()
  okCallBack: EventEmitter<any> = new EventEmitter<any>();
  constructor(public bsModalRef: BsModalRef, private router: Router) {}
 
  ngOnInit() {
  }
  Ok() {
    this.okCallBack.emit({});
    this.bsModalRef.hide();
  }
}