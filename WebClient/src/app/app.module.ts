import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule, MatFormFieldModule } from '@angular/material';
import { MatInputModule, MatSortModule, MatPaginatorModule } from '@angular/material';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ModalModule } from 'ngx-bootstrap/modal';

import { AuthServiceConfig, GoogleLoginProvider } from 'angularx-social-login';

import { SliderModule } from 'angular-image-slider';

import { NgxPaginationModule } from 'ngx-pagination';

import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AboutComponent } from './about/about.component';
import { BodyRoutingModule } from './body/body-routing.module';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './footer/footer.component';
import { NavComponent } from './nav/nav.component';
import { ConfirmDialogComponent } from './body/dialog/dialog.component';
import { HomeComponent } from './body/home/home.component';



const MaterialsComponents = [
  MatButtonModule,
  MatTableModule,
  MatFormFieldModule,
  MatInputModule,
  MatSortModule,
  MatPaginatorModule

];

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("659589669275-bkeko1ise012qmsrbl5h6663s99lf9es.apps.googleusercontent.com")
      }
    ]
  );
  return config;
}
@NgModule({
  declarations: [
    AppComponent,
    BodyComponent,
    NavComponent,
    FooterComponent,
 
    ConfirmDialogComponent,
    HomeComponent,
    AboutComponent,
   
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BodyRoutingModule,
    BrowserAnimationsModule,
    SliderModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    NgbModule,
    MaterialsComponents,
    NgxPaginationModule,
    ModalModule.forRoot(),
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger'
    }),
    
  ],
  exports: [MaterialsComponents],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: getAuthServiceConfigs,
  },
 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }